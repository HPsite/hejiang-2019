<?php
/**
 * Created by IntelliJ IDEA.
 * User: luwei
 * Date: 2017/7/18
 * Time: 19:13
 */

namespace app\modules\api\models;


use app\models\Goods;
use app\models\Option;
use app\models\Order;
use app\models\OrderDetail;
use app\models\OrderRefund;
use yii\data\Pagination;
use yii\helpers\VarDumper;

class OrderListForm extends Model
{
    public $store_id;
    public $user_id;
    public $status;
    public $page;
    public $limit;

    public function rules()
    {
        return [
            [['page', 'limit', 'status',], 'integer'],
            [['page',], 'default', 'value' => 1],
            [['limit',], 'default', 'value' => 20],
        ];
    }

    public function search()
    {
        if (!$this->validate())
            return $this->getModelError();
        if ($this->status > 4) {
            $query = Order::find()->where([
                'is_delete' => 0,
                'store_id' => $this->store_id,
                'user_id' => $this->user_id,
                'is_cancel' => 0,
                'type' => 3,
            ]);
        }else if(4 >= $this->status && $this->status >= 0){
            $query = Order::find()->where([
                'is_delete' => 0,
                'store_id' => $this->store_id,
                'user_id' => $this->user_id,
                'is_cancel' => 0,
                'type' => 0
            ]);
        }else{
            $query = Order::find()->where([
                'is_delete' => 0,
                'store_id' => $this->store_id,
                'user_id' => $this->user_id,
                'is_cancel' => 0,
                'type' => ['or', 0,3],
            ]);
        }
        if ($this->status == 0) {//待付款
            $query->andWhere([
                'is_pay' => 0,
            ]);
        }
        if ($this->status == 1) {//待发货
            $query->andWhere([
                'is_pay' => 1,
                'is_send' => 0,
            ]);
        }
        if ($this->status == 2) {//待收货
            $query->andWhere([
                'is_send' => 1,
                'is_confirm' => 0,
            ]);
        }
        if ($this->status == 3) {//已完成
            $query->andWhere([
                'is_confirm' => 1,
            ]);
        }
        if ($this->status == 4) {//售后订单
            return $this->getRefundList();
        }
        //我的租赁
        if ($this->status == 30) {//待付款
            $query->andWhere([
                'is_pay' => 0,
            ]);
        }
        if ($this->status == 31) {//已租==有押金
//            'is_back' => ['or', 0,1],
            $query = Order::find()->where([
                'is_pay' => 1,
                'is_delete' => 0,
                'store_id' => $this->store_id,
                'user_id' => $this->user_id,
                'is_cancel' => 0,
                'type' => 3,
                'is_back' => 0,
            ]);
            $query->andWhere([
                "and",
                'is_pay' => 1,
//                'is_back' => 0,
               ['or' ,
                   [
                       'is_send' => 1
                   ],
                   [
                       'is_send' => 0
                   ]
               ],
            ]);
        }
        if ($this->status == 33) {//已完成==退款完成，租赁 zl_true == 1 代表完成
            $query->andWhere([
                'is_confirm' => 1,
                'zl_true' => 1,
//                'is_back' => 1,
            ]);
        }
        $count = $query->count();
       // $commandQuery = clone $query;
       // echo $commandQuery->createCommand()->getRawSql();die;
        $pagination = new Pagination(['totalCount' => $count, 'page' => $this->page - 1, 'pageSize' => $this->limit]);
        /* @var Order[] $list */
        $list = $query->limit($pagination->limit)->offset($pagination->offset)->orderBy('addtime DESC')->all();
//                $commandQuery = clone $query;
//        echo $commandQuery->createCommand()->getRawSql();die;
        $new_list = [];
        foreach ($list as $order) {
            $order_detail_list = OrderDetail::findAll(['order_id' => $order->id, 'is_delete' => 0]);
            $goods_list = [];
            foreach ($order_detail_list as $order_detail) {
                $goods = Goods::findOne($order_detail->goods_id);
                if (!$goods)
                    continue;
                $goods_pic = isset($order_detail->pic) ? $order_detail->pic ?: $goods->getGoodsPic(0)->pic_url : $goods->getGoodsPic(0)->pic_url;
                $goods_list[] = (object)[
                    'goods_id' => $goods->id,
                    'goods_pic' => $goods_pic,
//                    'goods_pic' => $goods->getGoodsPic(0)->pic_url,
                    'goods_name' => $goods->name,
                    'num' => $order_detail->num,
                    'price' => $order_detail->total_price,
                    'attr_list' => json_decode($order_detail->attr),
                ];
            }
            $qrcode = null;
            $new_list[] = (object)[
                'order_id' => $order->id,
                'order_no' => $order->order_no,
                'addtime' => date('Y-m-d H:i', $order->addtime),
                'goods_list' => $goods_list,
                'total_price' => $order->total_price,
                'pay_price' => $order->pay_price,
                'is_pay' => $order->is_pay,
                'is_send' => $order->is_send,
                'is_confirm' => $order->is_confirm,
                'confirm_time' => $order->confirm_time,
                'pay_end_time' => $order->pay_end_time,
                'is_comment' => $order->is_comment,
                'apply_delete' => $order->apply_delete,
                'is_offline' => $order->is_offline,
                'qrcode' => $qrcode,
                'offline_qrcode' => $order->offline_qrcode,
                'express' => $order->express,
                'zl_true' => $order->zl_true,
                'back_order_time' => $order->back_order_time,
                'is_back' => $order->is_back,
                'time' => time(),
            ];
        }
        $payment = Option::get('payment', $this->store_id, 'admin', "{'wechat':'1'}");
        $payment = json_decode($payment, true);
        $balance = Option::get('re_setting',$this->store_id,'app');
        $balance = json_decode($balance,true);
        $pay_type_list = [];
        foreach($payment as $index=>$value){
            if($index == 'wechat' && $value == 1){
                $pay_type_list[] = [
                    'name'=>'微信支付',
                    'payment'=>0,
                ];
            }
            if($index == 'balance' && $value == 1 && $balance && $balance['status'] == 1){
                $pay_type_list[] = [
                    'name'=>'账户余额支付',
                    'payment'=>3,
                ];
            }
        }
        if(count($pay_type_list) == 0){
            $pay_type_list[] = [
                'name'=>'微信支付',
                'payment'=>0,
            ];
        }

        return [
            'code' => 0,
            'msg' => 'success',
            'data' => [
                'row_count' => $count,
                'page_count' => $pagination->pageCount,
                'list' => $new_list,
                'pay_type_list'=>$pay_type_list
            ],
        ];

    }

    private function getRefundList()
    {
        $query = OrderRefund::find()->alias('or')
            ->leftJoin(['od' => OrderDetail::tableName()], 'od.id=or.order_detail_id')
            ->leftJoin(['o' => Order::tableName()], 'o.id=or.order_id')
            ->where([
                'or.store_id' => $this->store_id,
                'or.user_id' => $this->user_id,
                'or.is_delete' => 0,
                'o.is_delete' => 0,
                'od.is_delete' => 0,
            ]);
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'page' => $this->page - 1, 'pageSize' => $this->limit]);
        $list = $query->select('o.id AS order_id,o.order_no,or.id AS order_refund_id,od.goods_id,or.addtime,od.num,od.total_price,od.attr,or.refund_price,or.type,or.status')->limit($pagination->limit)->offset($pagination->offset)->orderBy('or.addtime DESC')->asArray()->all();
        $new_list = [];
        foreach ($list as $item) {
            $goods = Goods::findOne($item['goods_id']);
            if (!$goods)
                continue;
            $new_list[] = (object)[
                'order_id' => intval($item['order_id']),
                'order_no' => $item['order_no'],
                'goods_list' => [(object)[
                    'goods_id' => intval($goods->id),
                    'goods_pic' => $goods->getGoodsPic(0)->pic_url,
                    'goods_name' => $goods->name,
                    'num' => intval($item['num']),
                    'price' => doubleval(sprintf('%.2f', $item['total_price'])),
                    'attr_list' => json_decode($item['attr']),
                ]],
                'addtime' => date('Y-m-d H:i', $item['addtime']),
                'refund_price' => doubleval(sprintf('%.2f', $item['refund_price'])),
                'refund_type' => $item['type'],
                'refund_status' => $item['status'],
                'order_refund_id' => $item['order_refund_id'],
            ];
        }
        return [
            'code' => 0,
            'msg' => 'success',
            'data' => [
                'row_count' => $count,
                'page_count' => $pagination->pageCount,
                'list' => $new_list,
            ],
        ];
    }

    public static function getCountData($store_id, $user_id)
    {
        $form = new OrderListForm();
        $form->limit = 0;
        $form->store_id = $store_id;
        $form->user_id = $user_id;
        $data = [];
//我的订单====
        $form->status = -1;
        $res = $form->search();
        $data['all'] = $res['data']['row_count'];
//        待付款
        $form->status = 0;
        $res = $form->search();
        $data['status_0'] = $res['data']['row_count'];
//        待发货
        $form->status = 1;
        $res = $form->search();
        $data['status_1'] = $res['data']['row_count'];
//        待收货
        $form->status = 2;
        $res = $form->search();
        $data['status_2'] = $res['data']['row_count'];
//        已完成
        $form->status = 3;
        $res = $form->search();
        $data['status_3'] = $res['data']['row_count'];
//我的租赁
//        待付款======租赁的 type == 3  例 下边的status
        $form->status = 30;
        $res = $form->search();
        $data['status_z_0'] = $res['data']['row_count'];
//        待发货====已租
        $form->status = 31;
        $res = $form->search();
        $data['status_z_1'] = $res['data']['row_count'];
//         已完成======意义是租赁的时间到期了，客户可以点击退款
        // 点击退款==填写订单号，和快递单号
        $form->status = 33;
        $res = $form->search();
        $data['status_z_3'] = $res['data']['row_count'];
        return $data;
    }

}